import requests
from bs4 import BeautifulSoup
import sqlite3
import threading
import queue

class PttCrawler:
    def __init__(self, database_file):
        self.database_file = database_file
        self.data_queue = queue.Queue()

    def create_tables(self):
        try:
            conn = sqlite3.connect(self.database_file)
            cursor = conn.cursor()
            
            create_table_article = '''
            CREATE TABLE IF NOT EXISTS article (
                id INTEGER PRIMARY KEY,
                title TEXT,
                textBody TEXT
            )
            '''
            cursor.execute(create_table_article)
            
            create_table_responseText = '''
            CREATE TABLE IF NOT EXISTS response (
                id INTEGER PRIMARY KEY,
                article_id INTEGER,
                responseText TEXT
            )
            '''
            cursor.execute(create_table_responseText)
            
            conn.commit()
            conn.close()
        except sqlite3.Error as e:
            print(f"SQLite错误：{e}")

    def get_ptt_text(self, moveurl, max_articles=40):
        index = 0
        while index < max_articles:
            rQ = requests.get(moveurl).text
            souP = BeautifulSoup(rQ, "html5lib")
            nextpage = "https://www.ptt.cc" + souP.find_all('a', "btn wide")[1]['href']
            moveurl = nextpage

            for mySoup in souP.find_all("div", "r-ent"):
                title = mySoup.find("div", "title").text.strip()
                if ("[movie]" in title) or ("[公告]" in title) or ("[版務" in title):
                    continue
                else:
                    try:
                        href = "https://www.ptt.cc" + mySoup.find("div", "title").a["href"]
                    except:
                        href = None
                    if href:
                        list_rq = requests.get(href).text
                        list_soup = BeautifulSoup(list_rq, "html5lib")
                        textBody = list_soup.find("div", "bbs-screen bbs-content")

                        for metaline in textBody.find_all("div", "article-metaline"):
                            metaline.decompose()
                        for metaline in textBody.find_all("div", "article-metaline-right"):
                            metaline.decompose()
                        for metaline in textBody.find_all("span", "f2"):
                            metaline.decompose()
                        response_texts = []
                        for response in textBody.find_all("div", "push"):
                            response_texts.append(response.text.strip())
                        # print(response_texts)
                        
                        for metaline in textBody.find_all("div", "push"):
                            metaline.decompose()
                        

                        textBody = textBody.text.strip()

                        self.data_queue.put((index, title, textBody, response_texts))
                        index += 1
                        if index >= max_articles:
                            break

    def process_data_thread(self):
        conn = sqlite3.connect(self.database_file)
        cursor = conn.cursor()

        while True:
            try:
                index, title, textBody, response_texts = self.data_queue.get(timeout=10)
                print(index)
                cursor.execute("INSERT INTO article (id, title, textBody) VALUES (?, ?, ?)", (index, title, textBody))
                # article_id = cursor.lastrowid

                for response_text in response_texts:
                    cursor.execute("INSERT INTO response (article_id, responseText) VALUES (?, ?)", (index, response_text))

                conn.commit()
            except queue.Empty:
                break
            except sqlite3.Error as e:
                print(f"SQLite错误：{e}")

        conn.close()

    def start_crawling(self, start_url, max_articles=40):
        self.create_tables()
        self.get_ptt_text(start_url, max_articles)

        data_thread = threading.Thread(target=self.process_data_thread)
        data_thread.start()

        

# 创建一个 PttCrawler 实例并启动爬取
crawler = PttCrawler("DB.db")
crawler.start_crawling("https://www.ptt.cc/bbs/movie/index.html", max_articles=40)
